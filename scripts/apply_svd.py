"""Apply SVD."""
import os

import counterix

if __name__ == '__main__':
    DIM = 10000
    PPMI_MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/ppmi/'  # replace this with your own env path
    models_filepaths = [os.path.join(PPMI_MODELS_DIRPATH, filename) for filename in
                        os.listdir(PPMI_MODELS_DIRPATH) if filename.endswith('.ppmi.npz')]
    for model_filepath in models_filepaths:
        counterix.svd(model_filepath, DIM)
