"""ACount words in corpus."""
import os
import functools
import multiprocessing

import counterix


def count(min_count, corpus_filepath):
    return counterix.count(min_count=min_count,
                           corpus_filepath=corpus_filepath)

if __name__ == '__main__':
    NUM_THREADS = 7
    MIN_COUNT = 2
    CORPORA_DIRPATH = '/home/debian/corpora/'
    # CORPORA_DIRPATH = '/Users/akb/Github/counterix/data/corpora/'
    corpora_filepaths = [os.path.join(CORPORA_DIRPATH, filename) for filename in
                         os.listdir(CORPORA_DIRPATH) if filename.endswith('txt')]
    with multiprocessing.Pool(NUM_THREADS) as pool:
        _count = functools.partial(count, MIN_COUNT)
        for _ in pool.imap_unordered(_count, corpora_filepaths):
            pass
