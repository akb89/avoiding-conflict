"""Check MEN and SIMLEX perf of SVD-TOP-300 models."""
import os
from collections import defaultdict

import numpy as np

import embeddix

if __name__ == '__main__':
    TOP = 300
    # MODELS_DIRPATH = '/home/debian/frontiers/models/svd/'
    MODELS_DIRPATH = '/Users/akb/Gitlab/frontiers/models/svd/'
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH) if filename.endswith('singvectors.npy')]
    results = {
        'men': {
            'top-1': defaultdict(str),
            'top-0': defaultdict(str)
        },
        'simlex': {
            'top-1': defaultdict(str),
            'top-0': defaultdict(str)
        }
    }
    for singvectors_filepath in models_filepaths:
        singvalues_filepath = '{}.singvalues.npy'.format(singvectors_filepath.split('.singvectors')[0])
        vocab_filepath = '{}.vocab'.format(singvectors_filepath.split('.ppmi')[0])
        singvectors = embeddix.load_dense(singvectors_filepath)[:, :TOP]
        singvalues = embeddix.load_dense(singvalues_filepath)[:TOP]
        vocab = embeddix.load_vocab(vocab_filepath)
        svd_top_alpha0 = singvectors
        svd_top_alpha1 = np.matmul(singvectors, np.diag(singvalues))
        corpus = os.path.basename(singvectors_filepath).split('.lower')[0]
        results['men']['top-1'][corpus] = embeddix.evaluate_word_similarity(svd_top_alpha1, vocab, 'men')
        results['men']['top-0'][corpus] = embeddix.evaluate_word_similarity(svd_top_alpha0, vocab, 'men')
        results['simlex']['top-1'][corpus] = embeddix.evaluate_word_similarity(svd_top_alpha1, vocab, 'simlex')
        results['simlex']['top-0'][corpus] = embeddix.evaluate_word_similarity(svd_top_alpha0, vocab, 'simlex')

    print('MEN\tWIKI07\tOANC\tWIKI2\tACL\tWIKI4\BNC\tWIKI')
    print('SVD-TOP~($\\alpha=1$) & {} & {} & {} & {} & {} & {} & {}'.format(
        results['men']['top-1']['wiki07'],
        results['men']['top-1']['oanc'],
        results['men']['top-1']['wiki2'],
        results['men']['top-1']['acl'],
        results['men']['top-1']['wiki4'],
        results['men']['top-1']['bnc'],
        results['men']['top-1']['wiki']))
    print('SVD-TOP~($\\alpha=0$) & {} & {} & {} & {} & {} & {} & {}'.format(
        results['men']['top-0']['wiki07'],
        results['men']['top-0']['oanc'],
        results['men']['top-0']['wiki2'],
        results['men']['top-0']['acl'],
        results['men']['top-0']['wiki4'],
        results['men']['top-0']['bnc'],
        results['men']['top-0']['wiki']))
    print('SIMLEX\tWIKI07\tOANC\tWIKI2\tACL\tWIKI4\BNC\tWIKI')
    print('SVD-TOP~($\\alpha=1$) & {} & {} & {} & {} & {} & {} & {}'.format(
        results['simlex']['top-1']['wiki07'],
        results['simlex']['top-1']['oanc'],
        results['simlex']['top-1']['wiki2'],
        results['simlex']['top-1']['acl'],
        results['simlex']['top-1']['wiki4'],
        results['simlex']['top-1']['bnc'],
        results['simlex']['top-1']['wiki']))
    print('SVD-TOP~($\\alpha=0$) & {} & {} & {} & {} & {} & {} & {}'.format(
        results['simlex']['top-0']['wiki07'],
        results['simlex']['top-0']['oanc'],
        results['simlex']['top-0']['wiki2'],
        results['simlex']['top-0']['acl'],
        results['simlex']['top-0']['wiki4'],
        results['simlex']['top-0']['bnc'],
        results['simlex']['top-0']['wiki']))
