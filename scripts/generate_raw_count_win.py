"""Generate raw count models with various window size for full English wiki."""
import functools
import multiprocessing

import counterix

if __name__ == '__main__':
    MIN_COUNT = 30
    WIN_SIZES = [1, 2, 5, 10, 15]
    NUM_THREADS = 5
    CORPUS_FILEPATH = '/home/debian/corpora/wiki.lower.tokenized.txt'  # replace this with your own env path
    with multiprocessing.Pool(NUM_THREADS) as pool:
        _generate = functools.partial(counterix.generate, CORPUS_FILEPATH, MIN_COUNT)
        for _ in pool.imap_unordered(_generate, WIN_SIZES):
            pass
