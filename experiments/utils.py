"""XP utils."""
import os
import multiprocessing
import random

import numpy as np
from scipy import stats
from tqdm import tqdm

import matrixor
import embeddix
import entropix


__all__ = ('MyPool', 'compute_corpora_seq_results', 'sample_dims',
           'compute_rmse_on_bins', 'compute_log_pearson', 'binize',
           'compute_win_pvals', 'compute_rmse_on_sampled_dim')


class NoDaemonProcess(multiprocessing.Process):
    @property
    def daemon(self):
        return False

    @daemon.setter
    def daemon(self, value):
        pass


class NoDaemonContext(type(multiprocessing.get_context())):
    Process = NoDaemonProcess


# We sub-class multiprocessing.pool.Pool instead of multiprocessing.Pool
# because the latter is only a wrapper function, not a proper class.
class MyPool(multiprocessing.pool.Pool):
    """Bypass multiprocessing Pool to run embedded multiproc."""
    def __init__(self, *args, **kwargs):
        kwargs['context'] = NoDaemonContext()
        super(MyPool, self).__init__(*args, **kwargs)


def _compute_pval(y, z, sample):
    x_sim = sample['test']['sim']
    y_left_vectors = y[sample['test']['left_idx']]
    y_right_vectors = y[sample['test']['right_idx']]
    y_sim = embeddix.similarity(y_left_vectors, y_right_vectors)
    z_left_vectors = z[sample['test']['left_idx']]
    z_right_vectors = z[sample['test']['right_idx']]
    z_sim = embeddix.similarity(z_left_vectors, z_right_vectors)
    xy = embeddix.spearman(x_sim, y_sim)
    xz = embeddix.spearman(x_sim, z_sim)
    yz = embeddix.spearman(y_sim, z_sim)
    n = len(x_sim)
    return embeddix.steiger_test_pval(xy, xz, yz, n)



def compute_win_seq_pval(x_model, y_model, x_samples, y_samples):
    pvalues = []
    for fold in x_samples:
        x_sampled = x_model[:, x_samples[fold]['dims']]
        y_sampled = y_model[:, y_samples[fold]['dims']]
        pvalues.append(_compute_pval(x_sampled, y_sampled, x_samples[fold]))
    return embeddix.hmean(pvalues)


def compute_win_top_pval(x_model, y_model, x_samples, y_samples):
    pvalues = []
    for fold in x_samples:
        x_sampled = x_model[:, :len(x_samples[fold]['dims'])]
        y_sampled = y_model[:, :len(y_samples[fold]['dims'])]
        pvalues.append(_compute_pval(x_sampled, y_sampled, x_samples[fold]))
    return embeddix.hmean(pvalues)


def compute_win_pvals(models_filepaths, results, samples):
    pvals = {'seq': {}, 'top': {}}
    for x in results.keys():
        pvals['seq'][x] = {}
        pvals['top'][x] = {}
        for y in results.keys():
            if x == y:
                pvals['seq'][x][y] = '-'
                pvals['top'][x][y] = '-'
            else:
                print('Computing pval on best-{} and {}'.format(x, y))
                for model_filepath in models_filepaths:
                    if x in model_filepath:
                        x_model = embeddix.load_dense(model_filepath)
                    if y in model_filepath:
                        y_model = embeddix.load_dense(model_filepath)
                pvals['seq'][x][y] = round(compute_win_seq_pval(
                    x_model, y_model, samples[x][results[x]['best']],
                    samples[y][results[x]['best']]), 4)
                pvals['top'][x][y] = round(compute_win_top_pval(
                    x_model, y_model, samples[x][results[x]['best']],
                    samples[y][results[x]['best']]), 4)
    return pvals


def compute_fair_pval(singvectors, sampling, singvalues=None):
    pvalues = []
    for fold in sampling:
        if singvalues is not None:
            svd_fair = np.matmul(singvectors[:, :len(sampling[fold]['dims'])],
                                 np.diag(singvalues[:len(sampling[fold]['dims'])]))
        else:
            svd_fair = singvectors[:, :len(sampling[fold]['dims'])]
        svd_sampled = singvectors[:, sampling[fold]['dims']]
        pvalues.append(_compute_pval(svd_sampled, svd_fair, sampling[fold]))
    return embeddix.hmean(pvalues)


def compute_fair_scores(singvectors, sampling, singvalues=None):
    test_scores = []
    for fold in sampling:
        if singvalues is not None:
            svd_fair = np.matmul(singvectors[:, :len(sampling[fold]['dims'])],
                                 np.diag(singvalues[:len(sampling[fold]['dims'])]))
        else:
            svd_fair = singvectors[:, :len(sampling[fold]['dims'])]
        test_scores.append(
            entropix.evaluate_on_splits(
                svd_fair, sampling[fold]['test'], 'spr'))
    return np.mean(test_scores), stats.sem(test_scores)


def compute_sampled_scores(singvectors, sampling):
    test_scores = [entropix.evaluate_on_splits(
        singvectors[:, sampling[fold]['dims']], sampling[fold]['test'], 'spr')
                   for fold in sampling.keys()]
    avg_spr = np.mean(test_scores)
    ste_spr = stats.sem(test_scores)
    avg_dim = np.mean([len(sampling[fold]['dims']) for fold in sampling.keys()])
    ste_dim = stats.sem([len(sampling[fold]['dims']) for fold in sampling.keys()])
    return avg_spr, ste_spr, avg_dim, ste_dim


def compute_window_seq_results(dataset, splits_dicts, kfold_size, metric,
                               max_num_threads, singvectors_filepath):
    corpus_name = 'win-{}'.format(
        os.path.basename(
            singvectors_filepath).split('.ppmi')[0].split('win-')[1])
    singvectors = embeddix.load_dense(singvectors_filepath)
    best_seq_spr_avg = 0
    samples = []
    best_splits_dict_num = -1
    for num, splits_dict in enumerate(splits_dicts):
        sampling = entropix.sample(
            model=singvectors, splits_dict=splits_dict, dataset=dataset,
            kfold_size=kfold_size, mode='seq', metric=metric, shuffle=True,
            max_num_threads=max_num_threads, limit=0)
        samples.append(sampling)
        spr_avg, spr_ste, dim_avg, dim_ste = compute_sampled_scores(
            singvectors, sampling)
        if spr_avg > best_seq_spr_avg:
            best_splits_dict_num = num
            best_seq_spr_avg = spr_avg
            best_seq_spr_ste = spr_ste
            best_seq_dim_avg = round(dim_avg)
            best_seq_dim_ste = round(dim_ste)
            top0_fair_avg, top0_fair_ste = compute_fair_scores(
                singvectors, sampling)
            top0_fair_pval = compute_fair_pval(singvectors, sampling)
    xp_results = {
        'best': best_splits_dict_num,
        'seq': {
            'spr': {
                'avg': round(best_seq_spr_avg, 2),
                'ste': round(best_seq_spr_ste, 2)
            },
            'dim': {
                'avg': round(best_seq_dim_avg),
                'ste': round(best_seq_dim_ste)
            }
        },
        'top-0': {
            'avg': round(top0_fair_avg, 2),
            'ste': round(top0_fair_ste, 2),
            'pval': round(top0_fair_pval, 4)
        }
    }
    return corpus_name, xp_results, samples


def compute_corpora_seq_results(dataset, num_runs, kfold_size, metric,
                                max_num_threads, singvectors_filepath):
    singvalues_filepath = '{}.singvalues.npy'.format(
        singvectors_filepath.split('.singvectors.aligned')[0])
    vocab_filepath = '{}.aligned.vocab'.format(
        singvectors_filepath.split('.ppmi')[0])
    corpus_name = os.path.basename(singvectors_filepath).split('.lower')[0]
    singvectors = embeddix.load_dense(singvectors_filepath)
    singvalues = embeddix.load_dense(singvalues_filepath)
    vocab = embeddix.load_vocab(vocab_filepath)
    best_seq_spr_avg = 0
    for _ in range(num_runs):
        splits_dict = entropix.load_splits_dict(dataset, vocab, kfold_size)
        sampling = entropix.sample(
            model=singvectors, splits_dict=splits_dict, dataset=dataset,
            kfold_size=kfold_size, mode='seq', metric=metric, shuffle=True,
            max_num_threads=max_num_threads, limit=0)
        spr_avg, spr_ste, dim_avg, dim_ste = compute_sampled_scores(
            singvectors, sampling)
        if spr_avg > best_seq_spr_avg:
            best_seq_spr_avg = spr_avg
            best_seq_spr_ste = spr_ste
            best_seq_dim_avg = round(dim_avg)
            best_seq_dim_ste = round(dim_ste)
            top1_fair_avg, top1_fair_ste = compute_fair_scores(
                singvectors, sampling, singvalues)
            top1_fair_pval = compute_fair_pval(singvectors, sampling,
                                               singvalues)
            top0_fair_avg, top0_fair_ste = compute_fair_scores(
                singvectors, sampling)
            top0_fair_pval = compute_fair_pval(singvectors, sampling)
    xp_results = {
        'seq': {
            'spr': {
                'avg': round(best_seq_spr_avg, 2),
                'ste': round(best_seq_spr_ste, 2)
            },
            'dim': {
                'avg': round(best_seq_dim_avg),
                'ste': round(best_seq_dim_ste)
            }
        },
        'top-1': {
            'avg': round(top1_fair_avg, 2),
            'ste': round(top1_fair_ste, 2),
            'pval': round(top1_fair_pval, 4)
        },
        'top-0': {
            'avg': round(top0_fair_avg, 2),
            'ste': round(top0_fair_ste, 2),
            'pval': round(top0_fair_pval, 4)
        }
    }
    return corpus_name, xp_results


def sample_dims(num_runs, dataset, sampling_mode, metric, limit,
                mode, singvectors_filepath):
    if mode not in ['corpora', 'window']:
        raise Exception('Unsupported mode: {}'.format(mode))
    if mode == 'corpora':
        vocab_filepath = '{}.aligned.vocab'.format(
            singvectors_filepath.split('.ppmi')[0])
        corpus_name = os.path.basename(singvectors_filepath).split('.lower')[0]
    elif mode == 'window':
        vocab_filepath = '{}.reduced.vocab'.format(
            singvectors_filepath.split('.ppmi')[0])
        corpus_name = 'win-{}'.format(
            os.path.basename(
                singvectors_filepath).split('.ppmi')[0].split('win-')[1])
    singvectors = embeddix.load_dense(singvectors_filepath)
    vocab = embeddix.load_vocab(vocab_filepath)
    mean = []
    median = []
    ninety = []
    shuffle = sampling_mode == 'seq'
    for _ in range(num_runs):
        splits_dict = entropix.load_splits_dict(dataset, vocab, kfold_size=0)
        sampling = entropix.sample(model=singvectors, splits_dict=splits_dict,
                                   dataset=dataset, kfold_size=0,
                                   mode=sampling_mode, metric=metric,
                                   shuffle=shuffle, max_num_threads=1,
                                   limit=limit)
        dims = sampling[1]['dims']
        mean.append(np.mean(dims))
        median.append(np.median(dims))
        ninety.append(np.percentile(dims, 90))
    if sampling_mode == 'seq':
        return corpus_name, {
            'median': {
                'avg': round(np.mean(median)),
                'ste': round(stats.sem(median)),
            },
            'mean': {
                'avg': round(np.mean(mean)),
                'ste': round(stats.sem(mean)),
            },
            'ninety': {
                'avg': round(np.mean(ninety)),
                'ste': round(stats.sem(ninety)),
            }
        }
    return corpus_name, {
        'median': round(np.mean(median)),
        'mean': round(np.mean(mean)),
        'ninety': round(np.mean(ninety))
    }


def compute_rmse_on_bins(bin_size, models_filepaths, pair):
    name1 = pair.split('-')[0]
    name2 = pair.split('-')[1]
    rmse = []
    for model_filepath in models_filepaths:
        if os.path.basename(model_filepath).startswith(name1):
            model1 = embeddix.load_dense(model_filepath)
        elif os.path.basename(model_filepath).startswith(name2):
            model2 = embeddix.load_dense(model_filepath)
    for idx in tqdm(range(model1.shape[1])):
        if idx % bin_size == 0:
            if idx + bin_size > model1.shape[1]:
                break
            m1 = model1[:, idx:idx+bin_size]
            m2 = model2[:, idx:idx+bin_size]
            rmse.append((idx, matrixor.align(m1, m2)))
    return pair, rmse


def compute_log_pearson(models_filepaths, pair):
    name1 = pair.split('-')[0]
    name2 = pair.split('-')[1]
    xcorrx = []
    for model_filepath in models_filepaths:
        if os.path.basename(model_filepath).startswith(name1):
            model1 = embeddix.load_dense(model_filepath)
        elif os.path.basename(model_filepath).startswith(name2):
            model2 = embeddix.load_dense(model_filepath)
    for col1, col2 in tqdm(zip(model1.T, model2.T), total=model1.shape[1]):
        xcorrx.append(embeddix.pearson(col1, col2))
    return np.abs(xcorrx)


def binize(data, bin_size):
    """Binning a numpy array."""
    return data[:(data.size // bin_size) * bin_size].reshape(-1, bin_size)


def compute_rmse_on_sampled_dim(metric, top, num_runs, models_filepaths, pair):
    name1 = pair.split('-')[0]
    name2 = pair.split('-')[1]
    for model_filepath in models_filepaths:
        if os.path.basename(model_filepath).startswith(name1):
            model1 = embeddix.load_dense(model_filepath)
            vocab1 = embeddix.load_vocab('{}.aligned.vocab'.format(model_filepath.split('.ppmi')[0]))
        elif os.path.basename(model_filepath).startswith(name2):
            model2 = embeddix.load_dense(model_filepath)
            vocab2 = embeddix.load_vocab('{}.aligned.vocab'.format(model_filepath.split('.ppmi')[0]))
    top1 = model1[:, :top]
    top2 = model2[:, :top]
    men_splits_dict_1 = entropix.load_splits_dict(dataset='men', vocab=vocab1, kfold_size=0)
    men_splits_dict_2 = entropix.load_splits_dict(dataset='men', vocab=vocab2, kfold_size=0)
    simlex_splits_dict_1 = entropix.load_splits_dict(dataset='simlex', vocab=vocab1, kfold_size=0)
    simlex_splits_dict_2 = entropix.load_splits_dict(dataset='simlex', vocab=vocab2, kfold_size=0)
    simverb_splits_dict_1 = entropix.load_splits_dict(dataset='simverb', vocab=vocab1, kfold_size=0)
    simverb_splits_dict_2 = entropix.load_splits_dict(dataset='simverb', vocab=vocab2, kfold_size=0)
    men_sample1 = entropix.sample(model=model1, splits_dict=men_splits_dict_1,
                                  dataset='men', kfold_size=0,
                                  mode='limit', metric=metric,
                                  shuffle=False, max_num_threads=1, limit=top)
    men_sample2 = entropix.sample(model=model2, splits_dict=men_splits_dict_2,
                                  dataset='men', kfold_size=0,
                                  mode='limit', metric=metric,
                                  shuffle=False, max_num_threads=1, limit=top)
    simlex_sample1 = entropix.sample(model=model1,
                                     splits_dict=simlex_splits_dict_1,
                                     dataset='simlex',
                                     kfold_size=0, mode='limit', metric=metric,
                                     shuffle=False, max_num_threads=1, limit=top)
    simlex_sample2 = entropix.sample(model=model2,
                                     splits_dict=simlex_splits_dict_2,
                                     dataset='simlex',
                                     kfold_size=0, mode='limit', metric=metric,
                                     shuffle=False, max_num_threads=1, limit=top)
    simverb_sample1 = entropix.sample(model=model1,
                                      splits_dict=simverb_splits_dict_1,
                                      dataset='simverb',
                                      kfold_size=0, mode='limit', metric=metric,
                                      shuffle=False, max_num_threads=1, limit=top)
    simverb_sample2 = entropix.sample(model=model2,
                                      splits_dict=simverb_splits_dict_2,
                                      dataset='simverb',
                                      kfold_size=0, mode='limit', metric=metric,
                                      shuffle=False, max_num_threads=1, limit=top)
    men1 = model1[:, men_sample1[1]['dims']]
    men2 = model1[:, men_sample2[1]['dims']]
    simlex1 = model1[:, simlex_sample1[1]['dims']]
    simlex2 = model1[:, simlex_sample2[1]['dims']]
    simverb1 = model1[:, simverb_sample1[1]['dims']]
    simverb2 = model1[:, simverb_sample2[1]['dims']]
    randoms = []
    for _ in range(num_runs):
        random1 = model1[:, random.sample(range(model1.shape[1]), top)]
        random2 = model2[:, random.sample(range(model1.shape[1]), top)]
        randoms.append(matrixor.align(random1, random2))
    return pair, {
        'random': {
            'avg': np.mean(randoms),
            'ste': stats.sem(randoms)
        },
        'top': matrixor.align(top1, top2),
        'men': matrixor.align(men1, men2),
        'simlex': matrixor.align(simlex1, simlex2),
        'simverb': matrixor.align(simverb1, simverb2)
    }
