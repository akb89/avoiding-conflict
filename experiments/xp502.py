"""Coordination is an interactive process: RMSE on diff size corpora.

np.dot in matrixor using multithreading by default with openblas.
To get full control over the number of threads used, launch this script
via env OMP_NUM_THREADS=1 python3 xp502.py
"""
import os
import functools

import utils
from utils import MyPool


if __name__ == '__main__':
    MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/aligned/'  # set this to your own corresponding directory
    PAIRS_LIST = ['wiki07-wiki2', 'wiki07-wiki4', 'wiki2-wiki4']
    BIN_SIZE = 250
    SCALE = 1e4
    NUM_THREADS = 3  # same as number of pairs
    results = {}
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH)
                        if filename.endswith('.singvectors.aligned.npy')]
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(
            utils.compute_rmse_on_bins, BIN_SIZE, models_filepaths)
        for pair, rmse in pool.imap_unordered(_compute_results, PAIRS_LIST):
            results[pair] = rmse
    for pair in PAIRS_LIST:
        with open('{}.dat'.format(pair), 'w', encoding='utf-8') as output_str:
            for idx, val in results[pair]:
                print('{}\t{}'.format(idx, val*SCALE), file=output_str)
