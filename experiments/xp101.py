"""Compute MEN, SIMLEX and SIMVERB scores for SVD-TOP and SVD-SEQ models.

For fair comparison, scores of TOP models are also computed on kfolds.

p-values are aggregated on each fold via Philipp Stinger's implementation of
the Steiger's (1980) test (following Rastogi et al 2015), available here:
https://github.com/psinger/CorrelationStats/blob/master/corrstats.py
p-values are then combined via equally weighted harmonic mean, following
the proposition of Wilson (2019) for combining p-values of dependent tests.

References are:

@inproceedings{rastogietal2015,
    title = "Multiview {LSA}: Representation Learning via Generalized {CCA}",
    author = "Rastogi, Pushpendre  and Van Durme, Benjamin  and Arora, Raman",
    booktitle = "Proceedings of the 2015 Conference of the North {A}merican Chapter of the Association for Computational Linguistics: Human Language Technologies",
    month = may # "{--}" # jun,
    year = "2015",
    address = "Denver, Colorado",
    publisher = "Association for Computational Linguistics",
    url = "https://www.aclweb.org/anthology/N15-1058",
    doi = "10.3115/v1/N15-1058",
    pages = "556--566",
}

@article{steiger1980,
  abstract = {In psychological research, it is desirable to be able to make statistical comparisons between correlation coefficients measured on the same individuals. For example, an experimenter (E) may wish to assess whether 2 predictors correlate equally with a criterion variable. In another situation, the E may wish to test the hypothesis that an entire matrix of correlations has remained stable over time. The present article reviews the literature on such tests, points out some statistics that should be avoided, and presents a variety of techniques that can be used safely with medium to large samples. Several numerical examples are provided. (18 ref) (PsycINFO Database Record (c) 2016 APA, all rights reserved)},
  address = {US},
  author = {Steiger, James H.},
  doi = {10.1037/0033-2909.87.2.245},
  issn = {1939-1455(Electronic),0033-2909(Print)},
  journal = {Psychological Bulletin},
  keywords = {*Literature Review,*Statistical Correlation,Statistical Tests},
  number = {2},
  pages = {245--251},
  publisher = {American Psychological Association},
  title = {{Tests for comparing elements of a correlation matrix.}},
  volume = {87},
  year = {1980}
}

@article{wilson2019,
  abstract = {Analysis of "big data" frequently involves statistical comparison of millions of competing hypotheses to discover hidden processes underlying observed patterns of data, for example, in the search for genetic determinants of disease in genome-wide association studies (GWAS). Controlling the familywise error rate (FWER) is considered the strongest protection against false positives but makes it difficult to reach the multiple testing-corrected significance threshold. Here, I introduce the harmonic mean p-value (HMP), which controls the FWER while greatly improving statistical power by combining dependent tests using generalized central limit theorem. I show that the HMP effortlessly combines information to detect statistically significant signals among groups of individually nonsignificant hypotheses in examples of a human GWAS for neuroticism and a joint human-pathogen GWAS for hepatitis C viral load. The HMP simultaneously tests all ways to group hypotheses, allowing the smallest groups of hypotheses that retain significance to be sought. The power of the HMP to detect significant hypothesis groups is greater than the power of the Benjamini-Hochberg procedure to detect significant hypotheses, although the latter only controls the weaker false discovery rate (FDR). The HMP has broad implications for the analysis of large datasets, because it enhances the potential for scientific discovery.},
  author = {Wilson, Daniel J.},
  doi = {10.1073/pnas.1814092116},
  edition = {2019/01/04},
  issn = {1091-6490},
  journal = {Proceedings of the National Academy of Sciences of the United States of America},
  keywords = {*big data,*false positives,*model averaging,*multiple testing,*p-values,Genome-Wide Association Study/methods,Hepacivirus/*genetics,Hepatitis C/*virology,Humans,Models, Statistical,Viral Load/*genetics},
  language = {eng},
  month = {jan},
  number = {4},
  pages = {1195--1200},
  publisher = {National Academy of Sciences},
  title = {{The harmonic mean p-value for combining dependent tests}},
  url = {https://pubmed.ncbi.nlm.nih.gov/30610179 https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6347718/},
  volume = {116},
  year = {2019}
}
"""
import os
import functools

import utils
from utils import MyPool


if __name__ == '__main__':
    MODELS_DIRPATH = '/home/debian/frontiers/models/aligned_after_svd/aligned/'  # set this to your own corresponding directory
    CORPUS_LIST = ['wiki07', 'oanc', 'wiki2', 'acl', 'wiki4', 'bnc']
    METRIC = 'both'
    DATASET = 'men'  # change to 'simlex' to run SIMLEX XP
    NUM_THREADS = 6  # 1 thread per model
    NUM_KFOLD_THREADS = 5  # 1 thread per fold in 5-fold
    NUM_RUNS = 10
    KFOLD_SIZE = .20  # 20%, i.e. 5-fold validation
    results = {}
    models_filepaths = [os.path.join(MODELS_DIRPATH, filename) for filename in
                        os.listdir(MODELS_DIRPATH)
                        if filename.endswith('.singvectors.aligned.npy')]
    with MyPool(NUM_THREADS) as pool:
        _compute_results = functools.partial(
            utils.compute_corpora_seq_results, DATASET, NUM_RUNS, KFOLD_SIZE,
            METRIC, NUM_KFOLD_THREADS)
        for corpus, _results in pool.imap_unordered(_compute_results,
                                                    models_filepaths):
            results[corpus] = _results

    print('-'*80)
    print('PRINT RESULTS ON {}'.format(DATASET.upper()))
    print('model & alpha & WIKI07 & OANC & WIKI2 & ACL & WIKI4 & BNC')
    print('TOP fair alpha=1 spr = {}'.format(
        ' & '.join(['{} $\\pm$ {}'
                    .format(results[corpus]['top-1']['avg'],
                            results[corpus]['top-1']['ste'])
                    for corpus in CORPUS_LIST])))
    print('TOP fair alpha=1 pval = {}'.format(
        ' & '.join(['{}'.format(results[corpus]['top-1']['pval'])
                    for corpus in CORPUS_LIST])))
    print('TOP fair alpha=0 spr = {}'.format(
        ' & '.join(['{} $\\pm$ {}'
                    .format(results[corpus]['top-0']['avg'],
                            results[corpus]['top-0']['ste'])
                    for corpus in CORPUS_LIST])))
    print('TOP fair alpha=0 pval = {}'.format(
        ' & '.join(['{}'.format(results[corpus]['top-0']['pval'])
                    for corpus in CORPUS_LIST])))
    print('SEQ spr = {}'.format(
        ' & '.join(['{} $\\pm$ {}'
                    .format(results[corpus]['seq']['spr']['avg'],
                            results[corpus]['seq']['spr']['ste'])
                    for corpus in CORPUS_LIST])))
    print('SEQ dim = {}'.format(
        ' & '.join(['{} $\\pm$ {}'
                    .format(results[corpus]['seq']['dim']['avg'],
                            results[corpus]['seq']['dim']['ste'])
                    for corpus in CORPUS_LIST])))
