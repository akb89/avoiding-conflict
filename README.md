# FRONTIERS

This repository accompanies the paper *Avoiding conflict: when speaker coordination does not require conceptual agreement*.

## Install dependencies
```shell
pip3 install numpy==1.16.0 scipy=1.5.1 counterix=1.2.2 embeddix=1.16.0 entropix=2.0.1 tqdm==4.35.0
```

## Experiments

Refer to the two tables below to find the experiment script corresponding to each Table and Figure reported in the paper. All scripts used for experiments can be found under `experiments`.

| Table | Experiment |
|-------|------------|
| 1 | 001 |
| 3 | 101 |
| 4 | 201 |
| S1 | 002 |
| S2 | 102 |
| S3 | 102|
| S4 | 202 |


| Figure | Experiment |
|-------|------------|
| 1 | 001 |
| 3 | 501 |
| 4 | 502 |
| 5 | 601 |
| 6 | 601 |
| 7 | 602 |
| S1 | 601 |
| S2 | 601 |
| S3 | 601 |
| S4 | 601 |
| S5 | 602 |
| S6 | 602 |
| S7 | 602 |
| S8 | 602 |

## Corpora
The paper relies on 4 distinct corpora: OANC, BNC, ACL and various samples of the English Wikipedia (see paper for details). Wikipedia dumps and samples are generated with [WiToKit](https://github.com/akb89/witokit) from the January 20 2019 dump of the English Wikipedia. All corpora are tokenized with [Polyglot](https://github.com/aboSamoor/polyglot) and lowercased. To download a bundle of all the lowercased and tokenized corpora:
```shell
wget backup.3azouz.net/corpora.7z
```

## Models

### Raw count-based models
To directly download raw-count models (unaligned), do:
```shell
wget backup.3azouz.net/raw.7z
```
Else, to generate a raw count-based model from a given corpus, run:
```shell
counterix generate \
  --corpus /abs/path/to/corpus/txt/file \
  --min-count min_count_value \
  --win-size window_size
```

For replication, and in order to generate all raw count models with a min-count of 3 and a win-size of 2 from the above corpora directory, run the `generate_raw_count_corpora.py` script.

To generate the raw count models for window sizes [1, 5, 10, 15] on the full English Wikipedia, run the `generate_raw_count_win.py` script.

### Apply PPMI weighing
To directly download PPMI-weighed raw-count models, do:
```shell
wget backup.3azouz.net/ppmi.7z
```

Else, for replication, run the `apply_ppmi_weighing.py` script.

### Run SVD
To directly download 10K SVD models for OANC/WIKI07/ACL/WIKI2/BNC/WIKI4 corpora:
```shell
wget backup.3azouz.net/svd.7z
```

Else, for replication, run the `apply_svd.py` script.

### Align vocabularies
To directly download aligned svd models (for all but the full WIKI model), do:
```shell
wget backup.3azouz.net/aligned.7z
```
Else, for replication, run the `align_svd_matrix_vocab.py` script.

### Window
To directly download all RAW/PPMI/SVD models computed on the full Wikipedia with window size in [1, 2, 5, 10, 15], do:
```shell
wget backup.3azouz.net/window.7z
```

Else, for replication, run all the aforementioned steps on the `wiki.lower.tokenized.txt` corpus.

**CAREFUL**
The above window files are very large. Since we only use them to compute scores on the MEN/SIMLEX/SIMVERB datasets, we also provide *reduced* version of those models with rows limited to the union of MEN/SIMLEX/SIMVERB vocabularies.
To download those models, do:
```shell
wget backup.3azouz.net/window-reduced.7z
```

Else, for replication, to obtain the reduced models from the original window models, run the `scripts/reduce_window_models_on_sim_dataset_vocab.py` script.

### Sampling models in limit mode
To directly download sampled models in limit mode:
```shell
wget backup.3azouz.net/limit.7z
```

### Discussion models
In the Discussion section we make use of models where the vocabularies (rows and columns) of the raw count matrices are aligned BEFORE running PPMI and SVD.

To download the corresponding aligned, ppmi-weighed, and svd-ed models, do:
```shell
wget backup.3azouz.net/disc-aligned.7z
```
```shell
wget backup.3azouz.net/disc-ppmi.7z
```
```shell
wget backup.3azouz.net/disc-svd.7z
```

Else, for replication, run the `align_raw_matrix_vocab.py` script on the RAW models and then apply PPMI-weighing and SVD following the above steps.
